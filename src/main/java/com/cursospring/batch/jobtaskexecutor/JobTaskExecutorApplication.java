package com.cursospring.batch.jobtaskexecutor;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.jobtaskexecutor"})
public class JobTaskExecutorApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobTaskExecutorApplication.class, args);
    }

}
