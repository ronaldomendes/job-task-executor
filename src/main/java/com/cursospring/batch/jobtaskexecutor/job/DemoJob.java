package com.cursospring.batch.jobtaskexecutor.job;

import com.cursospring.batch.jobtaskexecutor.dto.EmployeeDTO;
import com.cursospring.batch.jobtaskexecutor.mapper.EmployeeFileRowMapper;
import com.cursospring.batch.jobtaskexecutor.model.Employee;
import com.cursospring.batch.jobtaskexecutor.processor.EmployeeProcessor;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

import javax.sql.DataSource;

import static com.cursospring.batch.jobtaskexecutor.utils.Constants.AGE;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.EMAIL;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.EMPLOYEE_ID;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.FIRSTNAME;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.LASTNAME;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.QUALIFIER_NAME;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.STEP_NAME;

@Configuration
@AllArgsConstructor
public class DemoJob {

    private final JobBuilderFactory jobBuilderFactory;
    private final StepBuilderFactory stepBuilderFactory;
    private final EmployeeProcessor employeeProcessor;
    private final DataSource dataSource;

    @Qualifier(value = QUALIFIER_NAME)
    @Bean
    public Job demoFourJob() throws Exception {
        return jobBuilderFactory.get(QUALIFIER_NAME).start(stepOneDemoFour()).build();
    }

    @Bean
    public Step stepOneDemoFour() throws Exception {
        return stepBuilderFactory.get(STEP_NAME)
                .<EmployeeDTO, Employee>chunk(5)
                .reader(employeeReader())
                .processor(employeeProcessor)
                .writer(employeeDBWriterDefault())
                .taskExecutor(taskExecutor())
                .build();
    }

    @Bean
    @StepScope
    public Resource inputFileResource(@Value("#{jobParameters[fileName]}") final String fileName) throws Exception {
        return new ClassPathResource(fileName);
    }

    @Bean
    @StepScope
    public FlatFileItemReader<EmployeeDTO> employeeReader() throws Exception {
        FlatFileItemReader<EmployeeDTO> reader = new FlatFileItemReader<>();
        reader.setResource(inputFileResource(null));
        reader.setLineMapper(defineMapper());
        return reader;
    }

    private DelimitedLineTokenizer defineTokenizer() {
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
        lineTokenizer.setNames(EMPLOYEE_ID, FIRSTNAME, LASTNAME, EMAIL, AGE);
        // a adição do delimitador em vírgula é opcional, por padrão já é dessa forma
        lineTokenizer.setDelimiter(DelimitedLineTokenizer.DELIMITER_COMMA);
        return lineTokenizer;
    }

    private DefaultLineMapper<EmployeeDTO> defineMapper() {
        DefaultLineMapper<EmployeeDTO> lineMapper = new DefaultLineMapper<>();
        lineMapper.setLineTokenizer(defineTokenizer());
        lineMapper.setFieldSetMapper(new EmployeeFileRowMapper());
        return lineMapper;
    }

    @Bean
    public JdbcBatchItemWriter<Employee> employeeDBWriterDefault() {
        JdbcBatchItemWriter<Employee> itemWriter = new JdbcBatchItemWriter<>();
        itemWriter.setDataSource(dataSource);
        itemWriter.setSql("insert into tb_employee (employee_id, first_name, last_name, email, age) values (:employeeId, :firstName, :lastName, :email, :age)");
        itemWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        return itemWriter;
    }

    @Bean
    public TaskExecutor taskExecutor() {
        SimpleAsyncTaskExecutor simpleAsyncTaskExecutor = new SimpleAsyncTaskExecutor();
        simpleAsyncTaskExecutor.setConcurrencyLimit(5);
        return simpleAsyncTaskExecutor;
    }
}
