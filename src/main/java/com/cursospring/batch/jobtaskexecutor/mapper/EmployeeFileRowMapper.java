package com.cursospring.batch.jobtaskexecutor.mapper;

import com.cursospring.batch.jobtaskexecutor.dto.EmployeeDTO;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import static com.cursospring.batch.jobtaskexecutor.utils.Constants.AGE;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.EMAIL;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.EMPLOYEE_ID;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.FIRSTNAME;
import static com.cursospring.batch.jobtaskexecutor.utils.Constants.LASTNAME;

public class EmployeeFileRowMapper implements FieldSetMapper<EmployeeDTO> {

    @Override
    public EmployeeDTO mapFieldSet(FieldSet fieldSet) throws BindException {
        EmployeeDTO employee = new EmployeeDTO();
        employee.setEmployeeId(fieldSet.readString(EMPLOYEE_ID));
        employee.setFirstName(fieldSet.readString(FIRSTNAME));
        employee.setLastName(fieldSet.readString(LASTNAME));
        employee.setEmail(fieldSet.readString(EMAIL));
        employee.setAge(fieldSet.readInt(AGE));
        return employee;
    }
}
